# LCG Toolchains

[CMake][] [toolchain files][CMake toolchains] to build a standard CMake project on top of LCG releases.

## Usage
To enable a toolchain it's enough to add the option `-DCMAKE_TOOLCHAIN_FILE=path/to/the/toolchain.cmake` to the call to `cmake`, e.g.:
```bash
cmake -S . -B build -DCMAKE_TOOLCHAIN_FILE=/opt/lcg-toolchains/LCG_96b/x86_64-centos7-gcc9-opt.cmake
```

The toolchain code will create a few helper scripts in the build tree, under the directory `${CMAKE_BINARY_DIR}/toolchain`:
- wrappers for the compilers
- generic `wrapper` command

## Layout
The toolchain files are organized in one directory per supported LCG version
plus one directory ([`fragments`](fragments)) with the implementation details.

The LCG version directory contains one file for each LCG binary tag, and one
file for each special platform used by LHCb, which delegates to the
corresponding LCG binary tag.

The main LCG binary tag file delegates to LCG version specific files for the
configuration of the compiler, with the details in one file per specific version of
the compiler. Then a file from the directory [`fragments/packages`](fragments/packages)
is included for the specific settings for the external packages.

The special file [`fragments/compilation_flags.cmake`](fragments/compilation_flags.cmake)
is then included to set the LHCb specific compilation flags (e.g. architecture flags,
warnings settings, etc.).

## Customize the toolchains
By default the value of `LCG_PLATFORM` (from the name of the CMake file) is used to set
`LCG_ARCHITECTURE` and `LCG_OPTIMIZATION`, which are used to tune the compilation flags,
but they can be overridden from the command line, for example to build for a custom
architecture, not officially supported:
```bash
cmake -S . -B build -DLCG_ARCHITECTURE=skylake -DCMAKE_TOOLCHAIN_FILE=/opt/lcg-toolchains/LCG_96b/x86_64-centos7-gcc9-opt.cmake
```

## LCG layers
The special variants of the LCG toolchains called *LCG layers* cna be selected by setting
the variable `LCG_LAYER` before the call to `project()` in the CMakeLists.txt file, for example
on the command line:
```bash
cmake -S . -B build -DLCG_LAYER=LHCB_1 -DCMAKE_TOOLCHAIN_FILE=/opt/lcg-toolchains/LCG_96b/x86_64-centos7-gcc9-opt.cmake
```

## Toolchain for LCG nightly builds
Since LCG nightly builds change configuration very often, we cannot provide static toolchain files,
so a generic one is provided: [`special/lcg-nightly.cmake`](special/lcg-nightly.cmake).

To use it, it's mandatory to specify the file defining the LCG nightly slot, via the variable
`LCG_EXTERNALS_FILE` (either as CMake variable or environment variable):
```bash
cmake -S . -B build -DLCG_EXTERNALS_FILE=/cvmfs/sft-nightlies.cern.ch/lcg/nightlies/dev4/Mon/LCG_externals_x86_64-centos7-gcc9-opt.txt \
    -DCMAKE_TOOLCHAIN_FILE=/opt/lcg-toolchains/special/lcg-nightly.cmake
```

[CMake]: https://cmake.org
[CMake toolchains]: https://cmake.org/cmake/help/latest/manual/cmake-toolchains.7.html
