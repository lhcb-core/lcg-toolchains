###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

#[[.rst
Special toolchain for LCG nightly builds
========================================

Since the LCG nightly builds can change the listo of platforms and projects
without warning, this toolchain detects the settings given the path to the
special file ``LCG_externals_<platform>.txt`` file describing the build.

The path to the file can be specified with the CMake or the environment
variable ``LCG_EXTERNALS_FILE``.
]]

# Make sure the toolchain is included only once
# (to avoid double changes to the environment)
include_guard(GLOBAL)

cmake_policy(PUSH)
cmake_policy(SET CMP0007 NEW)

message(STATUS "Entering ${CMAKE_CURRENT_LIST_FILE}")

if(NOT DEFINED LCG_EXTERNALS_FILE AND "$ENV{LCG_EXTERNALS_FILE}" STREQUAL "")
  message(FATAL_ERROR "LCG_EXTERNALS_FILE not defined")
elseif(NOT DEFINED LCG_EXTERNALS_FILE)
  set(LCG_EXTERNALS_FILE "$ENV{LCG_EXTERNALS_FILE}" CACHE STRING "Path to LCG_external_<platform>.txt file")
endif()
# Make sure the environment variable is set, so that CMake temporary projects work
set(ENV{LCG_EXTERNALS_FILE} "${LCG_EXTERNALS_FILE}")

message(STATUS "Using ${LCG_EXTERNALS_FILE}")
file(STRINGS "${LCG_EXTERNALS_FILE}" _externals_data)
string(REPLACE "_externals_" "_generators_" _generators_file "${LCG_EXTERNALS_FILE}")
if(EXISTS "${_generators_file}")
  message(STATUS "... with generators from ${_generators_file}")
  file(STRINGS "${_generators_file}" _generators_data)
else()
  message(STATUS "... without generators")
  set(_generators_data)
endif()
set(LCG_EXTERNALS_DIRS)
foreach(_line IN LISTS _externals_data _generators_data)
  string(TOLOWER "${_line}" _lower_line)
  if(_line MATCHES "^PLATFORM:")
    if(NOT LCG_PLATFORM)
      string(SUBSTRING "${_line}" 9 -1 LCG_PLATFORM)
      string(STRIP "${LCG_PLATFORM}" LCG_PLATFORM)
    endif()
  elseif(_line MATCHES "^VERSION:")
    if(NOT LCG_VERSION)
      string(SUBSTRING "${_line}" 8 -1 LCG_VERSION)
      string(STRIP "${LCG_VERSION}" LCG_VERSION)
    endif()
  elseif(_line MATCHES "^COMPILER:")
    if(NOT LCG_EXTERNALS_COMPILER_NAME)
      string(SUBSTRING "${_line}" 9 -1 _line)
      string(STRIP "${_line}" _line)
      list(GET _line 0 LCG_EXTERNALS_COMPILER_NAME)
      list(GET _line 1 LCG_EXTERNALS_COMPILER_VERSION)
    endif()
  elseif(NOT _lower_line MATCHES "^(ccache|cmake|gaudi|geant4|git;|ninja|xenv)")
    if(_lower_line MATCHES "^python;")
      list(GET _line 2 _python_version)
      string(STRIP "${_python_version}" _python_version)
      string(REGEX REPLACE "\\.[^.]+\$" "" _python_version "${_python_version}")
      string(REGEX REPLACE "\\.[^.]+\$" "" GAUDI_USE_PYTHON_MAJOR "${_python_version}")
      set(GAUDI_USE_PYTHON_MAJOR ${GAUDI_USE_PYTHON_MAJOR} CACHE STRING "Major version of Python to use")
      list(GET _line 3 _python_home)
      string(STRIP "${_python_home}" _python_home)
    endif()
    list(GET _line 3 _line)
    string(STRIP "${_line}" _line)
    list(APPEND LCG_EXTERNALS_DIRS "${_line}")
  endif()
endforeach()

if(NOT DEFINED LCG_VERSION)
  message(STATUS "LCG_VERSION not defined")
endif()
if(NOT DEFINED LCG_PLATFORM)
  message(STATUS "LCG_PLATFORM not defined")
endif()
string(REPLACE "-" ";" _platform_bits "${LCG_PLATFORM}")
if(NOT DEFINED LCG_ARCHITECTURE)
  list(GET _platform_bits 0 LCG_ARCHITECTURE)
endif()
if(NOT DEFINED LCG_OS)
  list(GET _platform_bits 1 LCG_OS)
endif()
if(NOT DEFINED LCG_COMPILER)
  list(GET _platform_bits 2 LCG_COMPILER)
endif()
if(NOT DEFINED LCG_OPTIMIZATION)
  list(GET _platform_bits 3 LCG_OPTIMIZATION)
endif()

if(NOT DEFINED LHCB_PLATFORM)
  set(LHCB_PLATFORM ${LCG_PLATFORM})
endif()

string(REGEX REPLACE "-[^-]+\$" "" LCG_SYSTEM "${LCG_PLATFORM}")
string(REGEX REPLACE "-[^-]+\$" "" LCG_HOST "${LCG_SYSTEM}")

foreach(bit IN ITEMS VERSION ARCHITECTURE OS COMPILER OPTIMIZATION PLATFORM SYSTEM)
    message(STATUS "LCG_${bit} -> ${LCG_${bit}}")
endforeach()

set(LCG_releases_base "/cvmfs/lhcb.cern.ch/lib/lcg/releases" CACHE PATH "Where to look for LCG releases")
set(FRAGMENTS_DIR ${CMAKE_CURRENT_LIST_DIR}/../fragments)

#[[---.rst
Helper function to remove duplicated from a PATH-like variables
#---]]
macro(_dedup_env_path name)
  string(REPLACE ":" ";" _${name}_tmp "$ENV{${name}}")
  list(REMOVE_DUPLICATES _${name}_tmp)
  list(FILTER _${name}_tmp EXCLUDE REGEX "^$")
  string(REPLACE ";" ":" _${name}_tmp "${_${name}_tmp}")
  set(ENV{${name}} "${_${name}_tmp}")
  unset(_${name}_tmp)
endmacro()

include(${FRAGMENTS_DIR}/compiler/${LCG_HOST}-${LCG_EXTERNALS_COMPILER_NAME}${LCG_EXTERNALS_COMPILER_VERSION}.cmake)

# --- BEGIN: handling of declared externals directories
include(${FRAGMENTS_DIR}/packages/macros.cmake)
_init_from_env()
foreach(_dir IN LISTS LCG_EXTERNALS_DIRS)
  _add_lcg_entry("${_dir}")
  if(_dir MATCHES "/catboost/")
    set(_catboost_dir "${_dir}")
  endif()
endforeach()
if(_catboost_dir)
  if(EXISTS "${_catboost_dir}/catboost/libs/model_interface")
    list(PREPEND ld_library_path "${_catboost_dir}/catboost/libs/model_interface")
  elseif(EXISTS "${_catboost_dir}/catboost/lib/model_interface")
    list(PREPEND ld_library_path "${_catboost_dir}/catboost/lib/model_interface")
  endif()
endif()
_add_lbenv_workspace(${LHCB_PLATFORM})
_update_env()
_fix_pkgconfig_search()
_set_pythonhome("${_python_home}")

set(_INTELAMPLIFIER_ROOT /cvmfs/projects.cern.ch/intelsw/psxe/linux/x86_64/2019/vtune_amplifier_2019.4.0.597835)
if(EXISTS "${_INTELAMPLIFIER_ROOT}")
  list(APPEND CMAKE_PREFIX_PATH "${_INTELAMPLIFIER_ROOT}")
  set(GAUDI_USE_INTELAMPLIFIER TRUE CACHE BOOL "enable IntelAmplifier based profiler in Gaudi")
else()
  message(STATUS " IntelAmplifier directory ${_INTELAMPLIFIER_ROOT} not found, turning off profiler")
  set(GAUDI_USE_INTELAMPLIFIER FALSE CACHE BOOL "enable IntelAmplifier based profiler in Gaudi")
endif()
# --- END: handling of declared externals directories

_dedup_env_path(PATH)
_dedup_env_path(LD_LIBRARY_PATH)
_dedup_env_path(ROOT_INCLUDE_PATH)

message(STATUS "Writing ${CMAKE_BINARY_DIR}/toolchain/wrapper for ${CMAKE_SOURCE_DIR}")
file(WRITE ${CMAKE_BINARY_DIR}/toolchain/wrapper
"#!/bin/sh -e
export PATH=$ENV{PATH}
export LD_LIBRARY_PATH=$ENV{LD_LIBRARY_PATH}
export PYTHONPATH=$ENV{PYTHONPATH}
export PYTHONHOME=$ENV{PYTHONHOME}
export ROOT_INCLUDE_PATH=$ENV{ROOT_INCLUDE_PATH}

exec \"\$@\"
")
execute_process(COMMAND chmod a+x ${CMAKE_BINARY_DIR}/toolchain/wrapper)

include(${FRAGMENTS_DIR}/compilation_flags.cmake)

set(CMAKE_SYSTEM_NAME ${CMAKE_HOST_SYSTEM_NAME})
set(CMAKE_SYSTEM_PROCESSOR ${CMAKE_HOST_SYSTEM_PROCESSOR})
set(CMAKE_CROSSCOMPILING_EMULATOR ${CMAKE_BINARY_DIR}/toolchain/wrapper)

# Allow definition of rule wrappers from cache variables
foreach(_action IN ITEMS COMPILE LINK CUSTOM)
  if(DEFINED CMAKE_RULE_LAUNCH_${_action})
    set_property(GLOBAL PROPERTY RULE_LAUNCH_${_action} "${CMAKE_RULE_LAUNCH_${_action}}")
  endif()
endforeach()

cmake_policy(POP)
