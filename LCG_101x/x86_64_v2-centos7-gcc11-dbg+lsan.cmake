###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Make sure the toolchain is included only once
# (to avoid double changes to the environment)
include_guard(GLOBAL)
message(STATUS "Entering ${CMAKE_CURRENT_LIST_FILE}")

set(CMAKE_BUILD_TYPE LSan CACHE STRING
    "Choose the type of build, options are: None Debug Release RelWithDebInfo MinSizeRel ...")

set(LHCB_PLATFORM x86_64_v2-centos7-gcc11-dbg+lsan)
set(LCG_ARCHITECTURE x86_64_v2)
include(${CMAKE_CURRENT_LIST_DIR}/x86_64-centos7-gcc11-dbg.cmake)
