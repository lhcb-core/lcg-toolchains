There has been no release of bare LCG_102b, only LCG_102b_LHCB_Core, so the files
here are delegating to their LCG_102b_LHCB_Core counterparts after tweaking
LCG_VERSION and LCG_LAYER.
