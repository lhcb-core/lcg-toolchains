###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# generic helper to forward from LCG_102b to LCG_102b_LHCB_Core

include_guard(GLOBAL)

message(STATUS "Entering ${CMAKE_CURRENT_LIST_FILE}")

set(LCG_VERSION LCG_102b)
if("${LCG_LAYER}" STREQUAL "")
    set(LCG_LAYER LHCB_Core)
endif()
get_filename_component(_filename "${CMAKE_CURRENT_LIST_FILE}" NAME)
include(${CMAKE_CURRENT_LIST_DIR}/../LCG_102b_LHCB_Core/${_filename})
