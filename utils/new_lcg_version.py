#!/usr/bin/env python3
###############################################################################
# (c) Copyright 2020-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Script to generate the files required for a new LCG release.
"""

import json
import logging
import os
import re
import sys
from collections import defaultdict
from copy import deepcopy
from datetime import date
from pathlib import Path
from subprocess import check_call
from textwrap import dedent
from typing import Dict, Iterable, Mapping, Optional

LCG_META_INFO_ROOT = "https://lcgpackages.web.cern.ch/lcgpackages/lcg/meta/"
IGNORED_LCG_PACKAGES = {
    "ccache",
    "cmake",
    "gaudi",
    "geant4",
    "git",
    "hepmc3",
    "ninja",
    "xenv",
}
CORE_LAYER = "LHCB_Core"
ROCM_VERSIONS = {
    "centos7": "5.0.0",
    "el9": "5.4.3",
}


def parse_lcg_info_line(line):
    """
    Parse lines like ' KEY1: value1, KEY2: value2.1,value2.b,' into a dictionary

    >>> parse_lcg_info_line(' KEY1: value1, KEY2: value2.1,value2.b,')
    {'KEY1': 'value1', 'KEY2': 'value2.1,value2.b'}
    """
    import re

    exp = re.compile(r",? ([A-Z_][A-Z0-9_]*): ")

    # make sure the line starts and ends with spaces to comply with the regular expression
    line = " {} ".format(line.strip())

    pos = 0
    key = None
    data = {}
    m = exp.search(line)
    while m:
        if key and pos:
            data[key] = line[pos : pos + m.start()]
        key = m.group(1)
        pos += m.end()
        m = exp.search(line[pos:])
    if key and pos:
        data[key] = line[pos:].rstrip().rstrip(",")
    return data


def parse_lcg_info(lines):
    """
    Parse the lines of an LCG release metadata file (as an iterable object).
    """
    data = {"compiler": None, "packages": {}}
    for line in lines:
        if line.lstrip().startswith("#"):
            continue
        line = parse_lcg_info_line(line)

        if line.get("COMPILER"):
            data["compiler"] = line["COMPILER"]
        else:
            print(
                "strange entry observed:", line
            )  # this is to help debug weird entries

        deps = [d for d in line.get("DEPENDS", "").split(",") if d]
        deps.sort()
        data["packages"][line["NAME"]] = {
            "version": line["VERSION"],
            "directory": line["DIRECTORY"],
            "hash": line["HASH"],
            "platform": line["PLATFORM"],
            "dependencies": deps,
        }
        if line["NAME"] != line["DESTINATION"]:
            print(
                "strange entry observed:", line
            )  # this is to help debug weird entries

    return data


def get_lcg_info(
    version: str,
    platform: str,
    layer: Optional[str] = None,
    overrides: Optional[Mapping[str, Dict]] = None,
    excludes: Optional[Iterable[str]] = None,
):
    """
    Return the list of packages for a given LCG version and platform.
    """
    from urllib.request import urlopen

    url = "{base}LCG_{version}{suffix}_{platform}.txt".format(
        base=LCG_META_INFO_ROOT,
        version=version,
        platform=platform,
        suffix=("_" + layer) if layer else "",
    )
    logging.debug("getting info from %s", url)
    result = parse_lcg_info(line.decode("utf-8") for line in urlopen(url))
    if overrides:
        for name in overrides:
            data = overrides[name].copy()
            if not data.get("platform"):
                data["platform"] = platform
            result["packages"][name] = data
    if excludes:
        excludes = set(e.lower() for e in excludes)
        result["packages"] = {
            name: data
            for name, data in result["packages"].items()
            if name.lower() not in excludes
        }
    return result


def make_tree(tree, root=os.path.curdir, do_git=True):
    from collections.abc import Mapping

    for key in tree:
        path = os.path.join(root, key)
        if isinstance(tree[key], Mapping):
            if not os.path.isdir(path):
                os.makedirs(path)
            logging.debug("entering %s", path)
            make_tree(tree[key], path, do_git)
        elif isinstance(tree[key], Symlink):
            if os.path.exists(path) or os.path.islink(path):
                os.remove(path)
            os.symlink(str(tree[key]), path)
            if do_git:
                check_call(["git", "add", key], cwd=root)
        else:
            header = HEADER if not key.endswith(".json") else ""
            update = False
            with open(path, "a+") as f:
                f.seek(0)
                old_contents = f.read()
                update = tree[key] != old_contents[len(header) :]
                if update:
                    logging.debug("writing  %s", path)
                    # only write if the part besides the header (year) differs
                    f.seek(0)
                    f.truncate()
                    f.write(header)
                    f.write(tree[key])
            if update and do_git:
                check_call(["git", "add", key], cwd=root)


def special_platform_config(platform, base):
    """
    For a given special LHCb platform return special CMake code to be used in
    the aliasing toolchain file.
    """
    special_settings = []
    post_config = []
    architecture, os_id, compiler, opt = platform.split("-")
    CUDA_ROOTS = {}
    if architecture.startswith("x86_64"):
        CUDA_ROOTS = {
            "cuda11_4": f"/cvmfs/sft.cern.ch/lcg/releases/cuda/11.4-166ec/{base}",
            "cuda11_8": "/cvmfs/sft.cern.ch/lcg/releases/cuda/11.8-83fe0/x86_64-centos7-gcc11-opt",
            "cuda12_1": "/cvmfs/sft.cern.ch/lcg/contrib/cuda/12.1/x86_64-centos7",
            "cuda12_3": "/cvmfs/projects.cern.ch/lcg/releases/cuda/12.3/x86_64-linux",
            "cuda12_4": "/cvmfs/projects.cern.ch/lcg/releases/cuda/12.4/x86_64-linux",
        }
    elif architecture.startswith("aarch64") or architecture.startswith("armv8"):
        CUDA_ROOTS = {
            "cuda12_4": "/cvmfs/projects.cern.ch/lcg/releases/cuda/12.4/aarch64-linux",
        }
        special_settings.append('set(USE_TORCH FALSE CACHE BOOL "")\n')

    if "+" in architecture:
        pass  # nothing special to do
    if "+" in os_id:
        pass  # nothing special to do
    if "+" in compiler:
        flags = set(compiler.split("+")[1:])
        if "dd4hep" in flags:
            flags.remove("dd4hep")
            special_settings.append('set(USE_DD4HEP TRUE CACHE BOOL "")\n')
        if "detdesc" in flags:
            flags.remove("detdesc")
            special_settings.append('set(USE_DD4HEP FALSE CACHE BOOL "")\n')
        if "hip5" in flags and os_id in ROCM_VERSIONS:
            flags.remove("hip5")
            special_settings.append('set(TARGET_DEVICE "HID" CACHE STRING "")\n')
            rocm_version = ROCM_VERSIONS[os_id]
            post_config.extend(
                [
                    f"set(ROCM_PATH /cvmfs/lhcbdev.cern.ch/tools/rocm-{rocm_version})\n",
                    f"list(PREPEND CMAKE_PREFIX_PATH /cvmfs/lhcbdev.cern.ch/tools/rocm-{rocm_version}/hip)\n",
                ]
            )
        for flag in CUDA_ROOTS:
            if flag in flags:
                flags.remove(flag)
                special_settings.append('set(TARGET_DEVICE "CUDA" CACHE STRING "")\n')
                root = CUDA_ROOTS[flag]
                post_config.append(
                    f"""
list(PREPEND CMAKE_PREFIX_PATH {root})
set(ENV{{PATH}} "{root}/bin:$ENV{{PATH}}")
set(ENV{{LD_LIBRARY_PATH}} "{root}/lib64:$ENV{{LD_LIBRARY_PATH}}")
""".strip()
                )

        if flags:
            raise ValueError(f"invalid compiler flag(s): {list(flags)}")
    if "+" in opt:
        flags = set(opt.split("+")[1:])
        sanitizers = {
            f"{s.lower()}san": f"{s}San" for s in ["A", "L", "UB", "T", "ALUB"]
        }
        active_sanitizers = list(flags.intersection(sanitizers))
        if active_sanitizers:
            if len(active_sanitizers) > 1:
                raise ValueError(
                    "invalid sanitizer list in plaform, specify only one at a time"
                )
            special_settings.append(
                (
                    "set(CMAKE_BUILD_TYPE {} CACHE STRING\n"
                    '    "Choose the type of build, options are: '
                    'None Debug Release RelWithDebInfo MinSizeRel ...")\n'
                ).format(sanitizers[active_sanitizers[0]])
            )
        pass  # nothing special to do
    return "".join(special_settings), "".join(post_config)


def _entry_point(version, platform, lcg_base):
    if "=" in platform:
        platform, lcg_platform = platform.split("=", 1)
        architecture, _, _, opt = platform.split("-")
        special_settings, post_config = special_platform_config(platform, lcg_platform)
        return TOOLCHAIN_ALIAS.format(
            architecture=architecture,
            lcg_platform=lcg_platform,
            platform=platform,
            opt=opt,
            special_settings=special_settings,
            post_config=post_config,
        )
    else:
        core_layer = (
            f'if(NOT DEFINED LCG_LAYER)\n    set(LCG_LAYER "{CORE_LAYER}")\nendif()\n'
            if CORE_LAYER
            else ""
        )
        return TOOLCHAIN_ENTRY_POINT.format(
            version=version, platform=platform, lcg_base=lcg_base, core_layer=core_layer
        )


def _compiler(system, compiler):
    family, version = compiler.split()
    if family == "GNU":
        name = "gcc"
    elif family == "Clang":
        name = "clang"
    else:
        raise ValueError("unknown compiler {!r}".format(compiler))

    return Symlink("{}-{}{}.cmake".format(system.rsplit("-", 1)[0], name, version))


def _packages(lcg_info, platform, overrides):
    data = [
        "include(${CMAKE_CURRENT_LIST_DIR}/macros.cmake)",
        "_init_from_env()",
        (
            "set(_python_version {0}.{1})\n"
            'set(GAUDI_USE_PYTHON_MAJOR {0} CACHE STRING "Major version of Python to use")\n'
        ).format(*lcg_info["packages"]["Python"]["version"].split(".")),
    ]

    names = set(lcg_info["packages"])
    names.update(overrides)

    lcg_dirs = {
        name: LCG_PACKAGE_PATH.format(
            **(overrides.get(name) or lcg_info["packages"][name])
        )
        for name in names
    }

    data.extend('_add_lcg_entry("{}")'.format(d) for d in sorted(lcg_dirs.values()))
    # catboost is installed differently from any other package
    if "catboost" in lcg_dirs:
        # as of catboost 1.2 (LCG 104) the layout of the install directory changed
        # from "libs" to "lib", but still special
        data.append(
            'list(PREPEND ld_library_path "{}/catboost/{}/model_interface")'.format(
                lcg_dirs["catboost"],
                "libs"
                if lcg_info["packages"]["catboost"]["version"].startswith("0")
                else "lib",
            )
        )
    data.append("_add_lbenv_workspace(${LHCB_PLATFORM})")
    data.append("_update_env()")
    # as of LCG 99 (libgit2 1.0.1) we need to help pkg-config to find libgit2
    # because it is installed in lib64 istead of lib (and CMake cross compilation
    # ignores lib64 dirs)
    if "libgit2" in lcg_dirs:
        data.append(
            (
                "if(EXISTS {root}/lib64/pkgconfig)\n"
                '    set(ENV{{PKG_CONFIG_PATH}} "{root}/lib64/pkgconfig:$ENV{{PKG_CONFIG_PATH}}")\n'
                "endif()"
            ).format(root=lcg_dirs["libgit2"])
        )
    data.append("_fix_pkgconfig_search()")
    if "Python" in lcg_dirs:
        data.append('_set_pythonhome("{}")'.format(lcg_dirs["Python"]))
    if "fontconfig" in lcg_dirs:
        data.append(
            'set(ENV{{FONTCONFIG_PATH}} "{}/etc/fonts")'.format(lcg_dirs["fontconfig"])
        )
    data.append(
        INTELAMPLIFIER_ENTRY
        if platform.startswith("x86_64")
        else DISABLE_INTELAMPLIFIER
    )
    if (
        "Vc" in lcg_info["packages"]
        and lcg_info["packages"]["Vc"]["version"] == "1.4.4"
    ):
        data.append(
            dedent("""\
                if(CMAKE_VERSION VERSION_GREATER_EQUAL "3.28")
                    # Hide CMake 3.28 warnings in Vc (see https://github.com/VcDevel/Vc/pull/364)
                    set(CMAKE_POLICY_DEFAULT_CMP0153 OLD)
                endif()
                """)
        )
    return "\n".join(data)


class Symlink:
    def __init__(self, target: str):
        self.target = target

    def __str__(self):
        return self.target


def main():
    import argparse

    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description="Create required files for a given LCG release and platform",
        epilog=dedent("""\
        PLATFORM SYNTAX
        ---------------
        A platform argument can be:
        - a plain `<lcg_platform>` string as defined in LCG builds (e.g. `x86_64-centos7-gcc11-opt`).
        - an alias definition in the form of `<lhcb_platform>=[<lcg_platform>]` in which case
          the `<lhcb_platform>` text (e.g. `x86_64_v3-centos7-gcc11-opt+g`) will use the LCG
          binaries for `<lcg_platform>`. In most cases the `<lcg_platform>` can be omitted so that
          it is deduced from `<lhcb_platform>` (e.g. `x86_64_v3-centos7-gcc11-opt+g=` is equivalent
          to `x86_64_v3-centos7-gcc11-opt+g=x86_64-centos7-gcc11-opt`).
          Note that when requiring an alias both the alias and the base platforms are generated.
        - file delegation in the form of `@<filename>`, in which case the entry is replaced with
          the list of platforms in `<filename>`, one per line ignoring empty lines and lines
          starting by `#` (e.g. `@my_platforms.txt` reads from `my_platforms.txt).

        OVERRIDE ENTRY
        --------------
        Sometimes we have to override some of the entries in an LCG release with some custom builds
        or add entries that are not available in LCG.
        To do it we use the `--override` option passing to it an argument like

          `<name>/<version>-<hash>[/<platform>]`

        If the platform part is not specified, the entry is used for all requested platforms,
        otherwise it is used only for the specified platform (useful to be able to specify
        different version hashes for different platforms).

        An override can also be in the form `@<filename>`, in which case it is replaced by a list
        of overrides from the file `<filename>` (one per line ignoring empty lines and lines starting
        by `#`).
        """),
    )

    parser.add_argument("version", help="version of LCG to generate")
    parser.add_argument(
        "platform",
        nargs="*",
        help="platforms to generate, see 'PLATFORM SYNTAX' below for details",
    )
    parser.add_argument("--no-git", action="store_true", help="do not commit to git")
    parser.add_argument(
        "--lcg-base",
        default="/cvmfs/lhcb.cern.ch/lib/lcg/releases",
        help="where to find LCG installed packages",
    )
    parser.add_argument(
        "--layer",
        action="append",
        default=[],
        help="layers to add to the given LCG version",
    )
    parser.add_argument(
        "--override",
        action="append",
        default=[],
        help="override (or add) the entry for an external library, "
        "see 'OVERRIDE ENTRY' below for details",
    )
    parser.add_argument(
        "--exclude",
        action="append",
        default=[],
        help="ignore specific externals from LCG",
    )

    args = parser.parse_args()
    args.exclude.extend(IGNORED_LCG_PACKAGES)

    if args.version.startswith("LCG_"):
        parser.error("the version of LCG must not be prefixed with LCG_")
    root_dir = Path(f"LCG_{args.version}")

    logging.basicConfig(level=logging.DEBUG)

    def fill_alias_base(platform):
        if not platform.endswith("="):
            return platform
        platform = platform[:-1]
        # split the platform and drop the "+extra" options
        arch, os, comp, opt = [x.split("+", 1)[0] for x in platform.split("-")]
        if arch[-2] == "v":  # strip _vX suffix
            arch = arch[:-3]
        if opt == "do0":  # special unoptimized debug build
            opt = "dbg"
        return "{}={}".format(platform, "-".join([arch, os, comp, opt]))

    def expand_file_args(items: list[str], default_source: Optional[str] = None):
        """
        Helper to iterate over a list where some of the items may be
        references to files like `@my_file.txt`.
        """
        if default_source and not items and (root_dir / default_source).exists():
            logging.debug("using data from %s", root_dir / default_source)
            items = [f"@{root_dir / default_source}"]
        for item in items:
            if item.startswith("@"):
                with open(item[1:]) as f:
                    for line in f:
                        line = line.strip()
                        if line and not line.startswith("#"):
                            yield line
            else:
                yield item

    # resolve the special platform '@filename'
    platforms = set(expand_file_args(args.platform, default_source="platforms.txt"))

    # handle the implicit platform aliases 'platform='
    platforms = set(fill_alias_base(platform) for platform in platforms)

    # make sure that bases for aliased platforms are included
    platforms.update(
        platform.split("=", 1)[-1] for platform in list(platforms) if "=" in platform
    )

    # parse the override entries
    overrides = defaultdict(dict)
    for override in expand_file_args(args.override, default_source="overrides.txt"):
        m = re.match(
            r"^(?P<directory>.+?)/(?P<version>[^/]+)-(?P<hash>[0-9a-f]+)(?:/(?P<platform>.+))?$",
            override,
        )
        if not m:
            parser.error(f"invalid override entry '{override}'")
        overrides[m.group("platform")][m.group("directory")] = {
            k: m.group(k) for k in ("version", "hash", "platform", "directory")
        }
    # here we propagate to all known platforms the overrides with no explicit platform
    for platform in platforms:
        if "=" not in platform:
            # keep a copy of the platform specific overrides
            special = overrides.get(platform, {})
            # use the generic overrides as base (setting correctly the platform field)
            overrides[platform] = deepcopy(overrides.get(None, {}))
            for name in overrides[platform]:
                overrides[platform][name]["platform"] = platform
            # restore platform specific overrides
            overrides[platform].update(special)

    # get lcg info for every upstream platform
    args.layer = [
        layer for layer in expand_file_args(args.layer, default_source="layers.txt")
    ]
    layers_names = set(args.layer)
    layers_names.add(CORE_LAYER or "base")

    layers = {layer: {} for layer in layers_names}
    if (root_dir / "lcginfo.json").exists():
        layers.update(json.load(open(root_dir / "lcginfo.json")))
    for layer in layers_names:
        for platform in platforms:
            # if it's not a platform alias and we do not have info
            if "=" not in platform and platform not in layers[layer]:
                layers[layer][platform] = get_lcg_info(
                    args.version,
                    platform,
                    None if layer == "base" else layer,
                    excludes=args.exclude,
                )
    data = layers[CORE_LAYER or "base"]

    # subplatform string ignoring the optimization level
    systems = {
        "-".join(platform.split("-")[:-1]): data[platform]["compiler"]
        for platform in data
    }
    # dictionary of the values to use in the templates
    # this is a dictionary of directories and files to be created
    # keys corresponding to dictionaries map to directories, while keys
    # mapping to string represent files
    # - first we add the entry points
    output = {
        str(root_dir): {
            "{}.cmake".format(platform.split("=", 1)[0]): _entry_point(
                args.version, platform, args.lcg_base
            )
            for platform in platforms
        },
        "fragments": {
            "compiler": {
                "LCG_{version}-{system}.cmake".format(
                    version=args.version, system=system
                ): _compiler(system, compiler)
                for system, compiler in systems.items()
            },
            "packages": {
                "LCG_{version}-{platform}.cmake".format(
                    version=args.version + (f"_{CORE_LAYER}" if CORE_LAYER else ""),
                    platform=platform,
                ): _packages(data[platform], platform, overrides[platform])
                for platform in data
            },
        },
    }
    output["fragments"]["packages"].update(
        (
            "LCG_{version}_{layer}-{platform}.cmake".format(
                version=args.version, platform=platform, layer=layer
            ),
            _packages(layers[layer][platform], platform, overrides[platform]),
        )
        for layer in layers_names
        if layer not in (CORE_LAYER, "base")
        for platform in layers[layer]
    )
    output["LCG_{}".format(args.version)]["lcginfo.json"] = json.dumps(
        layers,
        indent=2,
        sort_keys=True,
    )
    make_tree(output, do_git=not args.no_git)
    if not args.no_git:

        def splitter(args, indent="    ", width=78, prefix=""):
            yield prefix + args[0]
            count = len(prefix) + len(args[0])
            for arg in args[1:]:
                if count + 2 + len(arg) >= width:
                    yield " \\\n" + prefix + indent
                    count = len(prefix) + len(indent)
                else:
                    yield " "
                    count += 1
                yield arg
                count += len(arg)

        check_call(
            [
                "git",
                "commit",
                "-m",
                "Added LCG {version}\n\ncommand:\n{command}".format(
                    version=args.version,
                    command="".join(splitter(sys.argv, prefix="    ")),
                ),
            ]
        )

    return 0


# --- Resources ---
HEADER = """###############################################################################
# (c) Copyright {year:4} CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# DO NOT EDIT: File generated by {filename}

""".format(
    year=date.today().year,
    filename=Path(__file__).name,
)
TOOLCHAIN_ENTRY_POINT = """# Make sure the toolchain is included only once
# (to avoid double changes to the environment)
include_guard(GLOBAL)

cmake_policy(PUSH)
cmake_policy(SET CMP0007 NEW)

message(STATUS "Entering ${{CMAKE_CURRENT_LIST_FILE}}")
if(NOT DEFINED LCG_VERSION)
  get_filename_component(LCG_VERSION "${{CMAKE_CURRENT_LIST_DIR}}" NAME)
endif()
if(NOT DEFINED LCG_PLATFORM)
  get_filename_component(LCG_PLATFORM "${{CMAKE_CURRENT_LIST_FILE}}" NAME_WE)
endif()
string(REPLACE "-" ";" _platform_bits "${{LCG_PLATFORM}}")
if(NOT DEFINED LCG_ARCHITECTURE)
  list(GET _platform_bits 0 LCG_ARCHITECTURE)
endif()
if(NOT DEFINED LCG_OS)
  list(GET _platform_bits 1 LCG_OS)
endif()
if(NOT DEFINED LCG_COMPILER)
  list(GET _platform_bits 2 LCG_COMPILER)
endif()
if(NOT DEFINED LCG_OPTIMIZATION)
  list(GET _platform_bits 3 LCG_OPTIMIZATION)
endif()
{core_layer}
if(NOT DEFINED LHCB_PLATFORM)
  set(LHCB_PLATFORM ${{LCG_PLATFORM}})
endif()

string(REGEX REPLACE "-[^-]+\\$" "" LCG_SYSTEM "${{LCG_PLATFORM}}")
string(REGEX REPLACE "-[^-]+\\$" "" LCG_HOST "${{LCG_SYSTEM}}")

if("${{LCG_LAYER}}" STREQUAL "")
  set(LCG_VERSION_SUFFIX "")
else()
  set(LCG_VERSION_SUFFIX "_${{LCG_LAYER}}")
endif()

foreach(bit IN ITEMS VERSION LAYER ARCHITECTURE OS COMPILER OPTIMIZATION PLATFORM SYSTEM)
  if(DEFINED LCG_${{bit}})
    message(STATUS "LCG_${{bit}} -> ${{LCG_${{bit}}}}")
  endif()
endforeach()

set(LCG_releases_base "{lcg_base}" CACHE PATH "Where to look for LCG releases")
set(FRAGMENTS_DIR ${{CMAKE_CURRENT_LIST_DIR}}/../fragments)

#[[---.rst
Helper function to remove duplicated from a PATH-like variables
#---]]
macro(_dedup_env_path name)
  string(REPLACE ":" ";" _${{name}}_tmp "$ENV{{${{name}}}}")
  list(REMOVE_DUPLICATES _${{name}}_tmp)
  list(FILTER _${{name}}_tmp EXCLUDE REGEX "^$")
  string(REPLACE ";" ":" _${{name}}_tmp "${{_${{name}}_tmp}}")
  set(ENV{{${{name}}}} "${{_${{name}}_tmp}}")
  unset(_${{name}}_tmp)
endmacro()

include(${{FRAGMENTS_DIR}}/compiler/${{LCG_VERSION}}-${{LCG_SYSTEM}}.cmake)
include(${{FRAGMENTS_DIR}}/packages/${{LCG_VERSION}}${{LCG_VERSION_SUFFIX}}-${{LCG_PLATFORM}}.cmake)

_dedup_env_path(PATH)
_dedup_env_path(LD_LIBRARY_PATH)
_dedup_env_path(ROOT_INCLUDE_PATH)

message(STATUS "Writing ${{CMAKE_BINARY_DIR}}/toolchain/wrapper for ${{CMAKE_SOURCE_DIR}}")
file(WRITE ${{CMAKE_BINARY_DIR}}/toolchain/wrapper
"#!/bin/sh -e
export PATH=$ENV{{PATH}}
export LD_LIBRARY_PATH=$ENV{{LD_LIBRARY_PATH}}
export PYTHONPATH=$ENV{{PYTHONPATH}}
export PYTHONHOME=$ENV{{PYTHONHOME}}
export ROOT_INCLUDE_PATH=$ENV{{ROOT_INCLUDE_PATH}}
export FONTCONFIG_PATH=$ENV{{FONTCONFIG_PATH}}

exec \\"\\$@\\"
")
execute_process(COMMAND chmod a+x ${{CMAKE_BINARY_DIR}}/toolchain/wrapper)

include(${{FRAGMENTS_DIR}}/compilation_flags.cmake)

set(CMAKE_SYSTEM_NAME ${{CMAKE_HOST_SYSTEM_NAME}})
set(CMAKE_SYSTEM_PROCESSOR ${{CMAKE_HOST_SYSTEM_PROCESSOR}})
set(CMAKE_CROSSCOMPILING_EMULATOR ${{CMAKE_BINARY_DIR}}/toolchain/wrapper)

# Allow definition of rule wrappers from cache variables
foreach(_action IN ITEMS COMPILE LINK CUSTOM)
  if(DEFINED CMAKE_RULE_LAUNCH_${{_action}})
    set_property(GLOBAL PROPERTY RULE_LAUNCH_${{_action}} "${{CMAKE_RULE_LAUNCH_${{_action}}}}")
  endif()
endforeach()

cmake_policy(POP)
"""

TOOLCHAIN_ALIAS = """# Make sure the toolchain is included only once
# (to avoid double changes to the environment)
include_guard(GLOBAL)
message(STATUS "Entering ${{CMAKE_CURRENT_LIST_FILE}}")

if(NOT DEFINED LHCB_PLATFORM)
    set(LHCB_PLATFORM {platform})
endif()
if(NOT DEFINED LCG_ARCHITECTURE)
    set(LCG_ARCHITECTURE {architecture})
endif()
if(NOT DEFINED LCG_OPTIMIZATION)
    set(LCG_OPTIMIZATION {opt})
endif()
{special_settings}include(${{CMAKE_CURRENT_LIST_DIR}}/{lcg_platform}.cmake)
{post_config}
"""

LCG_PACKAGE_PATH = "${{LCG_releases_base}}/{directory}/{version}-{hash}/{platform}"

_INTELAMPLIFIER_ROOT = "/cvmfs/projects.cern.ch/intelsw/psxe/linux/x86_64/2019/vtune_amplifier_2019.4.0.597835"

INTELAMPLIFIER_ENTRY = """
set(_INTELAMPLIFIER_ROOT {})
if(EXISTS "${{_INTELAMPLIFIER_ROOT}}")
  list(APPEND CMAKE_PREFIX_PATH "${{_INTELAMPLIFIER_ROOT}}")
  set(GAUDI_USE_INTELAMPLIFIER TRUE CACHE BOOL "enable IntelAmplifier based profiler in Gaudi")
else()
  message(STATUS " IntelAmplifier directory ${{_INTELAMPLIFIER_ROOT}} not found, turning off profiler")
  set(GAUDI_USE_INTELAMPLIFIER FALSE CACHE BOOL "enable IntelAmplifier based profiler in Gaudi")
endif()
""".format(_INTELAMPLIFIER_ROOT)

DISABLE_INTELAMPLIFIER = """
message(STATUS "IntelAmplifier not available for the current platform, turning off profiler")
set(GAUDI_USE_INTELAMPLIFIER FALSE CACHE BOOL "enable IntelAmplifier based profiler in Gaudi")
"""

if __name__ == "__main__":
    sys.exit(main())
