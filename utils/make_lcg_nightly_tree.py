#!/usr/bin/env python3
###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Script to generate the files required for LCG nightlies.
"""
import logging
from pathlib import Path
from datetime import date
from subprocess import check_call
import sys
import re


def main():
    from argparse import ArgumentParser

    parser = ArgumentParser(description="Create required files for LCG nightly builds")

    parser.add_argument("version", help="version of LCG slot to generate")
    parser.add_argument("platform", nargs="+", help="platforms to generate")
    parser.add_argument("--no-git", action="store_true", help="do not commit to git")
    parser.add_argument(
        "--lcg-base",
        default="/cvmfs/sft-nightlies.cern.ch/lcg/nightlies",
        help="where to find LCG nightlies",
    )

    args = parser.parse_args()

    if args.version.startswith("LCG_"):
        parser.error("the version of LCG must not be prefixed with LCG_")

    logging.basicConfig(level=logging.DEBUG)

    all_files = []
    root = Path(f"LCG_{args.version}")
    for platform in args.platform:
        architecture = platform.split("-", 1)[0]
        # FIXME: this is not correct but it suffices for now
        lcg_platform = re.sub(r"_v[1-4]", "", platform).replace("-do0", "-dbg").replace("armv8.1_a", "aarch64")
        extra_settings = ""
        if "+dd4hep" in lcg_platform:
            lcg_platform = lcg_platform.replace("+dd4hep", "")
            extra_settings += 'set(USE_DD4HEP TRUE CACHE BOOL "")\n'
        if "+detdesc" in lcg_platform:
            lcg_platform = lcg_platform.replace("+detdesc", "")
            extra_settings += 'set(USE_DD4HEP FALSE CACHE BOOL "")\n'

        if architecture.startswith("aarch64") or architecture.startswith("armv8"):
            extra_settings += 'set(USE_TORCH FALSE CACHE BOOL "")\n'

        root.mkdir(exist_ok=True)
        logging.debug("entering %s", root)
        with open(root / f"{platform}.cmake", "w") as f:
            f.write(HEADER)
            f.write(BASE_TEMPLATE.format(platform=platform))
        all_files.append(root / f"{platform}.cmake")
        logging.debug("wrote %s", all_files[-1])

        (root / "Today").mkdir(exist_ok=True)
        with open(root / "Today" / f"{platform}.cmake", "w") as f:
            f.write(HEADER)
            f.write(TODAY_TEMPLATE.format(platform=platform))
        all_files.append(root / "Today" / f"{platform}.cmake")
        logging.debug("wrote %s", all_files[-1])

        for day in ("Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"):
            (root / day).mkdir(exist_ok=True)
            with open(root / day / f"{platform}.cmake", "w") as f:
                f.write(HEADER)
                f.write(
                    DAY_TEMPLATE.format(
                        lcg_base=args.lcg_base,
                        slot=args.version,
                        day=day,
                        platform=platform,
                        architecture=architecture,
                        lcg_platform=lcg_platform,
                        extra_settings=extra_settings,
                    )
                )
            all_files.append(root / day / f"{platform}.cmake")
            logging.debug("wrote %s", all_files[-1])

    if not args.no_git:
        check_call(["git", "add"] + all_files)

        def splitter(args, indent="    ", width=78, prefix=""):
            yield prefix + args[0]
            count = len(prefix) + len(args[0])
            for arg in args[1:]:
                if count + 2 + len(arg) >= width:
                    yield " \\\n" + prefix + indent
                    count = len(prefix) + len(indent)
                else:
                    yield " "
                    count += 1
                yield arg
                count += len(arg)

        check_call(
            [
                "git",
                "commit",
                "-m",
                "Added LCG nightly {version}\n\ncommand:\n{command}".format(
                    version=args.version,
                    command="".join(splitter(sys.argv, prefix="    ")),
                ),
            ]
        )
    return 0


# --- Resources ---
HEADER = """###############################################################################
# (c) Copyright {year:4} CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# DO NOT EDIT: File generated by {filename}

# Make sure the toolchain is included only once
# (to avoid double changes to the environment)
include_guard(GLOBAL)
""".format(
    year=date.today().year,
    filename=Path(__file__).name,
)

DAY_TEMPLATE = """
message(STATUS "Entering ${{CMAKE_CURRENT_LIST_FILE}}")

set(LHCB_PLATFORM {platform})
set(LCG_ARCHITECTURE {architecture})
set(LCG_VERSION {slot}/{day})
{extra_settings}
set(LCG_nightlies_base "{lcg_base}" CACHE PATH "Where LCG nightlies are deployed")

set(LCG_EXTERNALS_FILE ${{LCG_nightlies_base}}/{slot}/{day}/LCG_externals_{lcg_platform}.txt)
include(${{CMAKE_CURRENT_LIST_DIR}}/../../special/lcg-nightly.cmake)
"""

TODAY_TEMPLATE = """
include(${{CMAKE_CURRENT_LIST_DIR}}/../../fragments/lcg-nightlies-helpers.cmake)
get_current_day(LCG_CURRENT_DAY)
include(${{CMAKE_CURRENT_LIST_DIR}}/../${{LCG_CURRENT_DAY}}/{platform}.cmake)
"""

BASE_TEMPLATE = """
include(${{CMAKE_CURRENT_LIST_DIR}}/Today/{platform}.cmake)
"""


if __name__ == "__main__":
    sys.exit(main())
