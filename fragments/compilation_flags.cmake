cmake_minimum_required(VERSION 3.15)

set(CMAKE_CONFIGURATION_TYPES Release Debug FastDebug RelWithDebInfo)
set(CMAKE_C_FLAGS_FASTDEBUG_INIT "-g -Og")
set(CMAKE_CXX_FLAGS_FASTDEBUG_INIT "-g -Og")
set(CMAKE_Fortran_FLAGS_FASTDEBUG_INIT "-g -Og")

# can't use the _INIT variables because CMAKE then appends "-O2 -g -DNDEBG"
set(CMAKE_C_FLAGS_RELWITHDEBINFO "-O3 -DNDEBUG -g" CACHE STRING "Flags used by the C compiler during RELWITHDEBINFO builds.")
set(CMAKE_CXX_FLAGS_RELWITHDEBINFO "-O3 -DNDEBUG -g" CACHE STRING "Flags used by the CXX compiler during RELWITHDEBINFO builds.")
set(CMAKE_Fortran_FLAGS_RELWITHDEBINFO "-O3 -DNDEBUG -g" CACHE STRING "Flags used by the Fortran compiler during RELWITHDEBINFO builds." )

# Options to fine tune the compilation flags
option(LCG_DIAGNOSTICS_COLOR "force colors in compiler diagnostics" OFF)
option(GAUDI_DIAGNOSTICS_COLOR "[deprecated] use LCG_DIAGNOSTICS_COLOR" OFF)

# Set build type
if(NOT CMAKE_BUILD_TYPE)
  if(LCG_OPTIMIZATION STREQUAL "opt")
    set(CMAKE_BUILD_TYPE Release)
  elseif(LCG_OPTIMIZATION STREQUAL "opt+g")
    set(CMAKE_BUILD_TYPE RelWithDebInfo)
  elseif(LCG_OPTIMIZATION STREQUAL "do0")
    set(CMAKE_BUILD_TYPE Debug)
  elseif(LCG_OPTIMIZATION STREQUAL "dbg")
    set(CMAKE_BUILD_TYPE FastDebug)
  endif()
  set(CMAKE_BUILD_TYPE ${CMAKE_BUILD_TYPE} CACHE STRING
    "Choose the type of build, options are: None Debug Release RelWithDebInfo MinSizeRel ...")
endif()

# Set common flags
set(_common_flags)
# - architecture
#   - use gcc convention (e.g. x86-64 instead of x86_64)
string(REPLACE "_" "-" LCG_ARCHITECTURE "${LCG_ARCHITECTURE}")
#   - microarchitecture levels
#     (workaround for gcc < 11.0, clang < 12.0 and any other compiler)
if(LCG_ARCHITECTURE MATCHES "^x86-64-v[2-4]"
        AND ((CMAKE_CXX_COMPILER_ID STREQUAL "GNU" AND CMAKE_CXX_COMPILER_VERSION VERSION_LESS "11.0")
            OR (CMAKE_CXX_COMPILER_ID STREQUAL "Clang" AND CMAKE_CXX_COMPILER_VERSION VERSION_LESS "12.0")
            OR NOT (CMAKE_CXX_COMPILER_ID STREQUAL "GNU" OR CMAKE_CXX_COMPILER_ID STREQUAL "Clang")))
    string(REPLACE "x86-64-v4" "x86-64-v3+avx512f+avx512bw+avx512cd+avx512dq+avx512vl" LCG_ARCHITECTURE "${LCG_ARCHITECTURE}")
    string(REPLACE "x86-64-v3" "x86-64-v2+avx+avx2+bmi+bmi2+f16c+fma+lzcnt+movbe+xsave" LCG_ARCHITECTURE "${LCG_ARCHITECTURE}")
    string(REPLACE "x86-64-v2" "x86-64+popcnt+sse3+sse4.1+sse4.2+ssse3+cx16+sahf" LCG_ARCHITECTURE "${LCG_ARCHITECTURE}")
endif()
#   - custom aliases
string(REPLACE "vecwid256" "prefer-vector-width=256" LCG_ARCHITECTURE "${LCG_ARCHITECTURE}")
#   - extract main arch and options
if(LCG_ARCHITECTURE MATCHES "\\+")
    string(REGEX MATCHALL "[^+]+" LCG_ARCHITECTURE_OPTIONS "${LCG_ARCHITECTURE}")
    # the first chunk is the actual architecture
    list(POP_FRONT LCG_ARCHITECTURE_OPTIONS LCG_ARCHITECTURE)
    list(TRANSFORM LCG_ARCHITECTURE_OPTIONS PREPEND "-m")
else()
    # no architecture option
    set(LCG_ARCHITECTURE_OPTIONS)
endif()
list(PREPEND LCG_ARCHITECTURE_OPTIONS "-march=${LCG_ARCHITECTURE}")
list(JOIN LCG_ARCHITECTURE_OPTIONS " " LCG_ARCHITECTURE_OPTIONS)

string(APPEND _common_flags " -fmessage-length=0 -pipe")
if(LCG_DIAGNOSTICS_COLOR OR GAUDI_DIAGNOSTICS_COLOR)
  string(APPEND _common_flags " -fdiagnostics-color")
endif()

set(_common_warnings all extra error=return-type)
set(_CXX_warnings write-strings pointer-arith overloaded-virtual non-virtual-dtor)
set(_C_warnings write-strings pointer-arith)

if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU"
        OR (CMAKE_CXX_COMPILER_ID STREQUAL "Clang" AND CMAKE_CXX_COMPILER_VERSION VERSION_GREATER_EQUAL "11.0"))
  list(APPEND _CXX_warnings suggest-override)
endif()

foreach(_lang IN ITEMS CXX C)
    list(PREPEND _${_lang}_warnings ${_common_warnings})
    list(TRANSFORM _${_lang}_warnings PREPEND "-W")
    list(PREPEND _${_lang}_warnings "-pedantic")
    list(JOIN _${_lang}_warnings " " _${_lang}_warnings)

    string(APPEND CMAKE_${_lang}_FLAGS_INIT
        " ${LCG_ARCHITECTURE_OPTIONS} ${_common_flags} ${_${_lang}_warnings}")
endforeach()
string(APPEND CMAKE_Fortran_FLAGS_INIT
    " ${LCG_ARCHITECTURE_OPTIONS} ${_common_flags}")

set(CMAKE_SHARED_LINKER_FLAGS_INIT "-Wl,--as-needed -Wl,--no-undefined")
set(CMAKE_MODULE_LINKER_FLAGS_INIT "-Wl,--as-needed -Wl,--no-undefined")

include(${CMAKE_CURRENT_LIST_DIR}/sanitizers/settings.cmake)

# handle Gaudi v20 compatibility mode in the same way as GaudiBuildFlags.cmake
if(NOT GAUDI_V21)
  if(GAUDI_V22)
    add_compile_definitions(GAUDI_V22_API)
  else()
    add_compile_definitions(GAUDI_V20_COMPAT)
  endif()
endif()

# Enable build of public headers in stand-alone mode by default (target `all`)
# if it was not set before (either on the command line or in the cache).
# See https://gitlab.cern.ch/rmatev/lb-stack-setup/-/issues/83#note_7762532
if(NOT DEFINED GAUDI_TEST_PUBLIC_HEADERS_BUILD)
  set(GAUDI_TEST_PUBLIC_HEADERS_BUILD TRUE CACHE BOOL "Execute a test build of all public headers in the global target 'all'")
endif()
