###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Options and settings for builds with sanitizers
# http://clang.llvm.org/docs/AddressSanitizer.html
# http://clang.llvm.org/docs/LeakSanitizer.html
# http://clang.llvm.org/docs/ThreadSanitizer.html
# http://clang.llvm.org/docs/UndefinedBehaviorSanitizer.html
# https://gcc.gnu.org/onlinedocs/gcc/Instrumentation-Options.html

# Make sure the toolchain is included only once
# (to avoid double changes to the environment)
include_guard(GLOBAL)

# compiler flags
foreach(_lang IN ITEMS CXX C Fortran)
    set(_common_flags "${CMAKE_${_lang}_FLAGS_FASTDEBUG_INIT} -fsanitize-recover=all -fno-omit-frame-pointer")
    set(CMAKE_${_lang}_FLAGS_ASAN_INIT "${_common_flags} -fsanitize=address")
    set(CMAKE_${_lang}_FLAGS_LSAN_INIT "${_common_flags} -fsanitize=leak")
    set(CMAKE_${_lang}_FLAGS_TSAN_INIT "${_common_flags} -fsanitize=thread")
    set(CMAKE_${_lang}_FLAGS_UBSAN_INIT "${_common_flags} -fsanitize=undefined")
    set(CMAKE_${_lang}_FLAGS_ALUBSAN_INIT "${_common_flags} -fsanitize=address -fsanitize=undefined")
    # NB: -fsanitize=leak is only for standalone leak detection mode and is thus not needed above.
    # The leak sanitizer is enabled at runtime with detect_leaks=1 in ASAN_OPTIONS.
endforeach()

# runtime sanitizers settings
set(SANITIZER_OPTIONS_ASAN "detect_leaks=0,alloc_dealloc_mismatch=0,halt_on_error=0,suppressions=${CMAKE_CURRENT_LIST_DIR}/asan.supp,new_delete_type_mismatch=0")
set(SANITIZER_OPTIONS_LSAN "print_suppressions=0,halt_on_error=0,suppressions=${CMAKE_CURRENT_LIST_DIR}/lsan.supp")
set(SANITIZER_OPTIONS_TSAN "print_suppressions=0,halt_on_error=0,ignore_interceptors_accesses=1,ignore_noninstrumented_modules=1,suppressions=${CMAKE_CURRENT_LIST_DIR}/tsan.supp")
set(SANITIZER_OPTIONS_UBSAN "print_stacktrace=1,print_suppressions=0,halt_on_error=0,suppressions=${CMAKE_CURRENT_LIST_DIR}/ubsan.supp")

# if we are using a special sanitizer build, we have to modify the environment
string(TOUPPER "${CMAKE_BUILD_TYPE}" _build_type_up)
if(_build_type_up MATCHES "^(A|L|T|UB)SAN$")
  string(TOLOWER "${CMAKE_BUILD_TYPE}" _sanitizer_name)
  string(APPEND RUN_SCRIPT_EXTRA_COMMANDS "
# Special setting for sanitizers.
#   note: PRELOAD_SANITIZER_LIB is variable used by gaudirun.py and by Gaudi testing scripts
export PRELOAD_SANITIZER_LIB=lib${_sanitizer_name}.so
export ${_build_type_up}_OPTIONS=\"${SANITIZER_OPTIONS_${_build_type_up}}\"
")

  # Ignore failures in genconf, to prevent sanitizer failures from stopping the build
  set(GAUDI_GENCONF_NO_FAIL TRUE)
endif()

if(_build_type_up STREQUAL "ALUBSAN")
    string(APPEND RUN_SCRIPT_EXTRA_COMMANDS "
# Special setting for sanitizers.
export PRELOAD_SANITIZER_LIB=libasan.so:libubsan.so
export ASAN_OPTIONS=\"detect_leaks=1,alloc_dealloc_mismatch=0,halt_on_error=0,suppressions=${CMAKE_CURRENT_LIST_DIR}/asan.supp,new_delete_type_mismatch=0\"
export LSAN_OPTIONS=\"${SANITIZER_OPTIONS_LSAN}\"
export UBSAN_OPTIONS=\"${SANITIZER_OPTIONS_UBSAN}\"
")
endif()

# FIXME new_delete_type_mismatch=0 is the only way we can diable the many errors from dd4hep, see https://gitlab.cern.ch/lhcb/LHCb/-/issues/307
