###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
set(LCG_COMPILER_VERSION 16.0.3-9dda8)
set(LCG_BINUTILS_VERSION 2.40-acaab)
set(LCG_CLANG_GCC_TOOLCHAIN 13.1.0)

include(${CMAKE_CURRENT_LIST_DIR}/binutils.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/clang.cmake)
