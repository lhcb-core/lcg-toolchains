###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
set(LCG_COMPILER_VERSION 11.1.0-e80bf)
set(LCG_BINUTILS_VERSION 2.36.1-a9696)

include(${CMAKE_CURRENT_LIST_DIR}/binutils.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/gcc.cmake)
