###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
include_guard(GLOBAL)

# This file contains what is needed to use GCC
string(REGEX REPLACE "-.*$" "" LCG_COMPILER_BASE_VERSION "${LCG_COMPILER_VERSION}")

# Define compiler cache variables
set(CMAKE_CXX_COMPILER ${CMAKE_BINARY_DIR}/toolchain/g++ CACHE FILEPATH "Path to C++ compiler")
set(CMAKE_C_COMPILER ${CMAKE_BINARY_DIR}/toolchain/gcc CACHE FILEPATH "Path to C compiler")
set(CMAKE_Fortran_COMPILER ${CMAKE_BINARY_DIR}/toolchain/gfortran CACHE FILEPATH "Path to Fortran compiler")
mark_as_advanced(CMAKE_CXX_COMPILER CMAKE_C_COMPILER CMAKE_Fortran_COMPILER)

# Preset compiler ID and version to use them in the toolchain (CMake detects them afterwards)
set(CMAKE_C_COMPILER_ID "GNU")
set(CMAKE_CXX_COMPILER_ID "GNU")
set(CMAKE_Fortran_COMPILER_ID "GNU")
set(CMAKE_C_COMPILER_VERSION "${LCG_COMPILER_BASE_VERSION}")
set(CMAKE_CXX_COMPILER_VERSION "${LCG_COMPILER_BASE_VERSION}")
set(CMAKE_Fortran_COMPILER_VERSION "${LCG_COMPILER_BASE_VERSION}")

# Set the compiler
set(COMPILER_ROOT ${LCG_releases_base}/gcc/${LCG_COMPILER_VERSION}/${LCG_HOST})
# Special variable pointing to the GCC installation path,
# useful when somebody has to pass it to clang-based tools
set(GCC_TOOLCHAIN_ROOT ${COMPILER_ROOT})
# ensure we have libstdc++ in the library search path of the targets
set_property(DIRECTORY ${CMAKE_SOURCE_DIR} APPEND PROPERTY LINK_OPTIONS $<HOST_LINK:-Wl$<COMMA>-rpath$<COMMA>${COMPILER_ROOT}/lib64>)

set(ENV{PATH} "${COMPILER_ROOT}/bin:$ENV{PATH}")
# FIXME: strip existing gcc|llvm|clang entries from LD_LIBRARY_PATH
set(ENV{LD_LIBRARY_PATH} "${COMPILER_ROOT}/lib64:$ENV{LD_LIBRARY_PATH}")
set(ENV{ROOT_INCLUDE_PATH} "${COMPILER_ROOT}/include/c++/${LCG_COMPILER_BASE_VERSION}:${COMPILER_ROOT}/include/c++/${LCG_COMPILER_BASE_VERSION}/x86_64-pc-linux-gnu:$ENV{ROOT_INCLUDE_PATH}")

_dedup_env_path(PATH)
_dedup_env_path(LD_LIBRARY_PATH)
_dedup_env_path(ROOT_INCLUDE_PATH)

# filter out path components inherited from the original environment
string(REGEX MATCHALL "${LCG_releases_base}[^:]+" _path_cleaned "$ENV{PATH}")
string(JOIN ":" _path_cleaned ${_path_cleaned})
string(REGEX MATCHALL "${LCG_releases_base}[^:]+" _ld_library_path_cleaned "$ENV{LD_LIBRARY_PATH}")
string(JOIN ":" _ld_library_path_cleaned ${_ld_library_path_cleaned})

# generate wrapper scripts for the compilers
foreach(_cmd IN ITEMS g++ gcc gfortran)
  message(STATUS "Writing ${CMAKE_BINARY_DIR}/toolchain/${_cmd} for ${CMAKE_SOURCE_DIR}")
  file(WRITE "${CMAKE_BINARY_DIR}/toolchain/${_cmd}"
"#!/bin/sh
export PATH=${_path_cleaned}
export LD_LIBRARY_PATH=${_ld_library_path_cleaned}
exec ${COMPILER_ROOT}/bin/${_cmd} \"\$@\"
")
  execute_process(COMMAND chmod a+x ${CMAKE_BINARY_DIR}/toolchain/${_cmd})
  execute_process(COMMAND touch --reference=${COMPILER_ROOT}/bin/${_cmd} ${CMAKE_BINARY_DIR}/toolchain/${_cmd})
endforeach()
