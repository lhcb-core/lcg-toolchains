###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
include_guard(GLOBAL)
# add custom binutils to the path if needed
if(NOT LCG_BINUTILS_VERSION STREQUAL "")
  set(ENV{PATH} "${LCG_releases_base}/binutils/${LCG_BINUTILS_VERSION}/${LCG_HOST}/bin:$ENV{PATH}")
endif()
