###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
include(${CMAKE_CURRENT_LIST_DIR}/common.cmake)

set(_INTELAMPLIFIER_ROOT /cvmfs/projects.cern.ch/intelsw/psxe/linux/x86_64/2019/vtune_amplifier_2019.4.0.597835)
if(EXISTS "${_INTELAMPLIFIER_ROOT}")
  list(APPEND CMAKE_PREFIX_PATH "${_INTELAMPLIFIER_ROOT}")
  set(GAUDI_USE_INTELAMPLIFIER TRUE CACHE BOOL "enable IntelAmplifier based profiler in Gaudi")
else()
  message(STATUS " IntelAmplifier directory ${_INTELAMPLIFIER_ROOT} not found, turning off profiler")
  set(GAUDI_USE_INTELAMPLIFIER FALSE CACHE BOOL "disable IntelAmplifier based profiler in Gaudi")
endif()
