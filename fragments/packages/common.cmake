###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# This file contains what is needed to setup the dependencies
# NB: path to Intel VTune Amplifier should be provided externally (by setting
# the environment variable CMAKE_PREFIX_PATH or -DCMAKE_PREFIX_PATH at configure time)

file(TO_CMAKE_PATH "$ENV{PATH}" path)
file(TO_CMAKE_PATH "$ENV{LD_LIBRARY_PATH}" ld_library_path)
file(TO_CMAKE_PATH "$ENV{ROOT_INCLUDE_PATH}" root_include_path)
set(pythonpath) # empty the PYTHONPATH

# Set the paths to the dependencies
file(GLOB libs LIST_DIRECTORIES true "${LCG_releases_base}/${LCG_VERSION}/*/*/${LCG_PLATFORM}") # */* = name/version
list(FILTER libs EXCLUDE REGEX "^${LCG_releases_base}.*(ninja|Gaudi)") # do not use ninja or Gaudi from there
foreach(lib IN LISTS libs)
    list(APPEND CMAKE_PREFIX_PATH ${lib})
    if(EXISTS ${lib}/bin)
        list(PREPEND path ${lib}/bin)
    endif()
    foreach(_suffix IN ITEMS lib lib64)
        if(EXISTS ${lib}/${_suffix})
            file(GLOB inner_files "${lib}/${_suffix}/*.so*")
            if(inner_files)
                list(PREPEND ld_library_path ${lib}/${_suffix})
            endif()
            file(GLOB inner_files "${lib}/${_suffix}/*.py")
            if(inner_files)
                list(PREPEND pythonpath ${lib}/${_suffix})
            endif()
        endif()
    endforeach()
    if(EXISTS ${lib}/include)
        list(PREPEND root_include_path ${lib}/include)
    endif()
endforeach()

# file(TO_NATIVE_PATH) does not transform ; to :
string(REPLACE ";" ":" path "${path}")
set(ENV{PATH} "${path}")
string(REPLACE ";" ":" ld_library_path "${ld_library_path}")
set(ENV{LD_LIBRARY_PATH} "${ld_library_path}")
string(REPLACE ";" ":" pythonpath "${pythonpath}")
set(ENV{PYTHONPATH} "${pythonpath}")
string(REPLACE ";" ":" root_include_path "${root_include_path}")
set(ENV{ROOT_INCLUDE_PATH} "${root_include_path}")
# tuning of cmake delegation to pkg-config (https://cmake.org/cmake/help/latest/module/FindPkgConfig.html)
# if we let FindPkgConfig rely on CMAKE_PREFIX_PATH we will miss either lib or lib64 subdirs
# - make sure pkg-config uses CMAKE_PREFIX_PATH
set(PKG_CONFIG_USE_CMAKE_PREFIX_PATH YES)
# - when crosscompiling, CMake ignores /usr/lib64/pkgconfig, but this is kind of a hybrid
if(EXISTS /usr/lib64/pkgconfig AND NOT EXISTS ${CMAKE_BINARY_DIR}/toolchain/lib/pkgconfig)
    file(MAKE_DIRECTORY ${CMAKE_BINARY_DIR}/toolchain/lib/pkgconfig)
    file(GLOB _system_pc_files RELATIVE /usr/lib64/pkgconfig "/usr/lib64/pkgconfig/*.pc")
    foreach(_system_pc_file IN LISTS _system_pc_files)
        file(CREATE_LINK /usr/lib64/pkgconfig/${_system_pc_file} ${CMAKE_BINARY_DIR}/toolchain/lib/pkgconfig/${_system_pc_file} SYMBOLIC)
    endforeach()
endif()
if(EXISTS ${CMAKE_BINARY_DIR}/toolchain/lib/pkgconfig)
    list(APPEND CMAKE_PREFIX_PATH ${CMAKE_BINARY_DIR}/toolchain)
endif()

# PYTHONHOME needed by gdb
list(FILTER libs INCLUDE REGEX "${LCG_releases_base}/${LCG_VERSION}/Python/[^/]+/${LCG_PLATFORM}")
list(GET libs 0 pythonhome)
set(ENV{PYTHONHOME} "${pythonhome}")

# prevent find_package(Python) too look into the path
set(Python_ROOT_DIR "${pythonhome}")
set(Python2_ROOT_DIR "${pythonhome}")
set(Python3_ROOT_DIR "${pythonhome}")

# Before all we make sure we set the CMake python lookup policy to use the first
# one found in the path (i.e. from LCG)
set(Python_FIND_STRATEGY LOCATION)
