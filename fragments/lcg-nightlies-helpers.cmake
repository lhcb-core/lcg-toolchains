###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

#[=======[.rst
.. command:: get_current_day

  .. code-block:: cmake

    get_current_day(<variable>)

  This macro set ``<variable>`` to the current day abbreviated name (in C/POSIX locale).

  #]=======]
macro(get_current_day variable)
    execute_process(
        COMMAND env LC_ALL=C date +%a
        OUTPUT_VARIABLE ${variable}
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )
endmacro()

