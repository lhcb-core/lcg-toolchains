###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# generic helper to forward from LCG_101a to LCG_101a_LHCB_7

include_guard(GLOBAL)

message(STATUS "Entering ${CMAKE_CURRENT_LIST_FILE}")

set(LCG_VERSION LCG_101a)
set(LCG_LAYER LHCB_7)
get_filename_component(_filename "${CMAKE_CURRENT_LIST_FILE}" NAME)
include(${CMAKE_CURRENT_LIST_DIR}/../LCG_101a_LHCB_7/${_filename})
