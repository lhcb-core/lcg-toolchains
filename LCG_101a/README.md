There has been no release of bare LCG_101a, only LCG_101a_LHCB_7, so the files
here are delegating to their LCG_101a_LHCB_7 counterparts after tweaking
LCG_VERSION and LCG_LAYER.
